var socket = io();

socket.on('incrementBooth', function(swipeData) {
	var boothName = swipeData.boothName.replace(/ /g,'');
	var count = swipeData.count;
	$('#'+boothName+'count').text(count);
})

socket.on('updateTotal', function(total) {
		var elementId = '#'+total.name.replace(/ /g,'');
		$(elementId).text(total.count);
	
})

function navigate() {
	window.location.href = '/results';
}

function generateReport() {
	socket.emit('generateReport');
	$("#showResultsBtn").hide();
	$("#randomize").show();
}

function randomize() {

	$("#resultsContainer").children().each(function() {
		//$(this).css('color', 'red');
		$(this).delay(900).slideUp(3000);
	});

	$("#randomize").hide();
	$("#drawWinnerBtn").show();
}

function drawWinner() {

	var elements = $("#resultsContainer").children();
	var randomValue = elements[Math.floor(Math.random() * elements.length)];

	$(randomValue).show();
	$(randomValue).addClass('btn btn-block btn-outline btn-rounded btn-success');
}

socket.on('stream', function(userCount) {
	if (3 > userCount.count >= 1 ) {
		$( "#resultsContainer" ).append( "<div class='col-sm-2 text-info' >" + userCount.id.replace('swipeCount','') + " :<b>"+ userCount.count + "</b></div>" );
	}else if(userCount.count >= 3) {
		$( "#resultsContainer" ).append( "<div class='col-sm-2 text-success' >" + userCount.id.replace('swipeCount','') + " : <b>"+ userCount.count + "</div>" );
	}
});



$(document).ready(function(totals) {
	socket.emit('getTotals');

	socket.on('updateTotal', function(total) {
		var elementId = '#'+total.name.replace(/ /g,'');
	
		$(elementId).text(total.count);

		
	});


});