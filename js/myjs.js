var socket = io();

function getParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

socket.on('redirect', function(url){
	window.location.href = url;
})


//user is "finished typing," do something
function doneTyping () {
    //get input
	var swipeData = $('#focus').val();
	var boothName = getParameterByName('username');
	var req = {'swipeData': swipeData, 'boothName': boothName};
	socket.emit('newSwipe', req);

    // clear box
    $('#focus').val('');

}

function auth() {

	var pass = $('#password').val();
	var user = $('#username').val();

	var loginDetails = {'user': user, 'pass': pass};
	socket.emit('auth', loginDetails);
}

$(document).ready(function() {

	var boothName = getParameterByName('username');

	 $('#boothName').text(boothName);

	$(window).keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  return false;
		}
	});

    $("#focus").focus().bind('blur', function() {
        $(this).focus();            
    }); 

    $("html").click(function() {
        $("#focus").val($("#focus").val()).focus();
    });  

    //disable the tab key
    $(document).keydown(function(objEvent) {
        if (objEvent.keyCode == 9) {  //tab pressed
            objEvent.preventDefault(); // stops its action
       }
    });

     //setup before functions
	var typingTimer;                //timer identifier
	var doneTypingInterval = 300;  //time in ms (1 seconds)

	//on keyup, start the countdown
	$('#focus').keyup(function(){
	    clearTimeout(typingTimer);
	    if ($('#focus').val()) {
	        typingTimer = setTimeout(doneTyping, doneTypingInterval);
	    }
	});




});

