var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);
/*var AWS = require('aws-sdk');*/
var redis = require('redis');
var redisClient = redis.createClient();

redisClient.flushdb();

redisClient.on('connect', function(){
	console.log('connected to redis');
});

// consume new swipe
io.on('connection', function(client) {
	console.log('user joined');

	client.on('newSwipe', function(data) {
		/*var regex = /\^(.*?)\^/;*/
		var regex = /\%(.*?)\?/;
		if (regex.exec(data.swipeData) !== null) {
			var name = regex.exec(data.swipeData)[1];
		}else {
			var name = 'unknown';
		}
		console.log('name is ==: ' + name);
		// increment booth count
		redisClient.incr(data.boothName+'count');

		redisClient.get(data.boothName+'count', function(err, reply) {
			if (!err) {
				var swipeData = {'boothName': data.boothName, 'count': reply};
				client.broadcast.emit('incrementBooth', swipeData);
			}
		})

		//check if username 
		redisClient.get(name+'swipeCount', function(err, reply) {
			// first swipe
			console.log(reply);
			if (!err) {
				if (reply != null) {
					redisClient.incr(name+'swipeCount');
					console.log('Incrementing ==> ' + name);
				}else {
					console.log('First swipe for ==> '+name);
					redisClient.set(name+'swipeCount', 1);					
				}
			}
		})
	});

	client.on('auth', function(data) {
		//admin login, redirect to admin
		if (data.pass == 'tasc2017admin' && data.user == 'tascadmin') {
			redisClient.set(data.user, 'authenticated');
			client.emit('redirect', '/central?username=' + data.user);
		}else if(data.pass == 'letmein2017') {
			// store user and state
			redisClient.set(data.user, 'authenticated');
			redisClient.set(data.user+'count', 0);
			client.emit('redirect', '/index?username=' + data.user);
		}else {
			redisClient.set(data.user, 'notAuthenticated');
			client.emit('redirect', '/login');
		}
	})


	client.on('getTotals', function() {
		var keys = [];
		// get keys
		redisClient.keys('*count', function(err, reply) {
			if (!err) {
				keys = reply;
			}
			console.log('keys1 ==> '+ keys);

			// for each key
			keys.forEach(function(key) {
				// get key count
				var booth = {};
				booth['name'] = key
				redisClient.get(key, function(err, reply) {
					booth['count'] = reply;
					client.emit('updateTotal', booth);
				})
				
			})
		})
	})

	client.on('generateReport', function() {
		console.log('generating report');
		//get all user keys
		redisClient.keys('*swipeCount', function(err, reply) {
			if (!err) {
				keys = reply;
			}

			console.log('There are these keys: ' + keys);

			keys.forEach(function(key){
				redisClient.get(key, function(err, reply) {
					if (!err) {
						//totals[key] = reply;
						var userCount = {};
						userCount['id'] = key;
						userCount['count'] = reply;

						client.emit('stream', userCount);
					}
				})

				//console.log('totals array ' + JSON.stringify(totals));
			})
/*todo: find a way to wait until array is full so we can gererate the report page*/
			
		})
	})
});

function isAuthenticated(req, res, next) {
	var user = req.param('username');
	if (user) {
		redisClient.get(user, function(err, reply) {
			if (reply == 'authenticated') {
				return next();
			}
		});
	}else {
		res.redirect('/login')
	}
}


app.use("/js", express.static(__dirname + '/js'));
app.use("/css", express.static(__dirname + '/css'));
app.use("/images", express.static(__dirname + '/images'));
app.use("/bootstrap", express.static(__dirname + '/bootstrap'));
app.use("/plugins", express.static(__dirname + '/plugins'));
app.use("/node_modules", express.static(__dirname + '/node_modules'));
app.use("/less", express.static(__dirname + '/less'));

app.get('/index',isAuthenticated, function(req, res) {
    res.sendFile(__dirname + '/index.html');
});

app.get('/central',isAuthenticated, function(req, res) {
    res.sendFile(__dirname + '/html/central.html');
});

app.get('/login', function(req, res) {
    res.sendFile(__dirname + '/html/login.html');
});

app.get('/results', function(req, res) {
    res.sendFile(__dirname + '/html/results.html');
});

server.listen(1336, function() {
	console.log('App listening on port 1336!')
});